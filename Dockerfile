FROM debian
RUN apt update
RUN apt install nodejs npm -y
COPY ./nodeweb /app/
WORKDIR /app/
EXPOSE 8000
CMD node index.js